package model;

public class Abekatten {

	private String name;


	public Abekatten(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
