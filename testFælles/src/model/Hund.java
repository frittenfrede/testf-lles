package model;

public class Hund {
	private String name;
	private boolean chipped;
	private boolean chipId;

	public Hund(String name, boolean chipId) {
		this.name = name;
		this.chipId = chipId;
		this.chipped = true;
	}

	public Hund(String name) {
		this.name = name;
		this.chipped = false;
	}
}
